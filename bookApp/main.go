package main

import "fmt"

func main() {

	json1 := jsonReadCall()

	// fmt.Println(json1)
	fmt.Printf("%-10v %-10v %v", "Title", "Author", "Rating\n")
	for i := 0; i <= len(json1)-1; i++ {
		fmt.Printf("%-10s %-10s %g\n", json1[i].Title, json1[i].Author, json1[i].Rating)

	}

    title, series, author, genre, isbn, pages, date, read, rating := jsonReadStdin()
    jsonWriteCall(title, series, author, genre, isbn, pages, date, read, rating)

	json1 = jsonReadCall()

	// fmt.Println(json1)
	fmt.Printf("%-10v %-10v %v", "Title", "Author", "Rating\n")
	for i := 0; i <= len(json1)-1; i++ {
		fmt.Printf("%-10s %-10s %g\n", json1[i].Title, json1[i].Author, json1[i].Rating)

	}
}
