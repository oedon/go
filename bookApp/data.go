package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type bookData struct {
	Title  string  `json:"Title"`
	Series string  `json:"Series"`
	Author string  `json:"Author"`
	Genre  string  `json:"Genre"`
	ISBN   int64   `json:"ISBN"`
	Pages  int64   `json:"Pages"`
	Date   string  `json:"Date"`
	Read   bool    `json:"Read"`
	Rating float32 `json:"Rating"`
}

func jsonReadCall() []bookData {
	file := "myFile.json"
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	var jsonCall []bookData
	json.Unmarshal([]byte(data), &jsonCall)
	return jsonCall
}

func jsonWriteCall(title, series, author, genre string, isbn, pages int64, date string, read bool, rating float64) {
	filename := "myFile.json"
	err := checkFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	data := []bookData{}
	json.Unmarshal(file, &data)
	newStruct := &bookData{
		Title:  title,
		Series: series,
		Author: author,
        Genre: genre,
		ISBN:   isbn,
		Pages:  pages,
		Date:   date,
		Read:   read,
		Rating: float32(rating),
	}

	data = append(data, *newStruct)

	dataBytes, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(filename, dataBytes, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func jasonUpdateCall()  {

    
}

func jsonReadStdin() (string, string, string, string, int64, int64, string, bool, float64) {
	var title, series, author, genre, date string
	var isbn, pages int64
	var rating float64
	var read bool

	fmt.Printf("Title: ")
	title = inputScanner()
	fmt.Printf("Series: ")
	series = inputScanner()
	fmt.Printf("Author: ")
	author = inputScanner()
	fmt.Printf("Genre: ")
	genre = inputScanner()
	fmt.Printf("ISBN: ")
	fmt.Scan(&isbn)
	fmt.Printf("Pages: ")
	fmt.Scan(&pages)
	fmt.Printf("Date: ")
	date = inputScanner()
	fmt.Printf("Read: ")
	fmt.Scan(&read)
	fmt.Printf("Rating: ")
	fmt.Scan(&rating)

	return title, series, author, genre, isbn, pages, date, read, rating
}

func inputScanner() string {
	var value string
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		value = scanner.Text()
	}
	return value
}

func checkFile(filename string) error {
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		_, err := os.Create(filename)
		if err != nil {
			return err
		}
	}
	return nil
}
