package main

import (
	// "fmt"
	"fmt"
	"os/exec"
	"time"
)

func main() {
	// var copyCmd *exec.Cmd

	for {
		out, err := exec.Command("xclip", "-o").Output()

		if err != nil {
			panic(err)
		}

        out_str := fmt.Sprintf("%s", out)

        fmt.Println(out_str)
	

		time.Sleep(1 * time.Second)
	}

}
