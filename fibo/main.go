package main

import (
	"fmt"
)

func main() {
	var input int
	var fibo_ls []int
    fmt.Println("Calculate Fibonacci Sequence")
	fmt.Printf("Input n: ")
	fmt.Scan(&input)

	for i := 1; i < input + 1; i++ {
		fibo_ls = append(fibo_ls, fibo(i))
	}
    for idx, value := range fibo_ls{
        fmt.Printf("%v => %v\n", idx+1, value)
    }

}

func fibo(input int) int {
	if input == 0 {
		return 0
	} else if input == 1 {
		return 1
	} else {
		return fibo(input-1) + fibo(input-2)
	}
}
