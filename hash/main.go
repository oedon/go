package main

import (
	"crypto/sha256"
	"fmt"
	"log"

	"golang.org/x/crypto/ssh/terminal"
)

func main() {
	// var input int
	// fmt.Printf("Input String to hash: ")
    // Read password without showing
	// x, err := terminal.ReadPassword(input)
	// if err != nil {
	// 	log.Fatal(err)
	// }
 //    fmt.Println()
	// fmt.Printf("SHA256: %x\n", sha256.Sum256(x))
 //    
 //    y := &x
 //    fmt.Println(string(*y))
 //    // Clear password byte slice
 //    x = []byte{}
	// fmt.Printf("SHA256: %x\n", sha256.Sum256(x))
 //    fmt.Println(*y)
 //    fmt.Println(y)
    enc := readPassword()
    fmt.Printf("%x\n", enc)


}

func readPassword() [32]byte {
	var input int
	fmt.Printf("Input String to hash: ")
    // Read password without showing
	x, err := terminal.ReadPassword(input)
	if err != nil {
		log.Fatal(err)
	}
    enc := sha256.Sum256(x)
    x = []byte{}
    return enc
}
