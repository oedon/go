package main

import (
	"fmt"
	"log"
	"os"
	// "strconv"
)

func main() {
	x := 5
	y := 6
	val := x > y
	wd, _ := os.Getwd()
	hn, _ := os.Hostname()
	data, err := os.ReadFile("test.txt")

	fmt.Printf("%t\n", val)
	fmt.Printf("%v\n", wd)
	fmt.Printf("%v\n", hn)

	if err != nil {
		log.Fatal(err)
	}
	os.Stdout.Write(data)
}
