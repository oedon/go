package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main()  {
    scanner := bufio.NewScanner(os.Stdin)
    fmt.Printf("Type Year: ")
    scanner.Scan()
    input, _ := strconv.ParseInt(scanner.Text(), 10, 64)
    fmt.Printf("You are %d old years.\n", 2022 - input)
    
}
