package main

import (
	"fmt"
	"net/http"
    "io/ioutil"
)

func main() {
    url1, url2 := "https://euro.rate.sx/1ada", "https://euro.rate.sx/1xmr"

	req1, err := http.NewRequest("GET", url1, nil)
    isError(err)
	
    req2, err := http.NewRequest("GET", url2, nil)
    isError(err)

	res1, err := http.DefaultClient.Do(req1)
    isError(err)
	
    res2, err := http.DefaultClient.Do(req2)
    isError(err)
	
    defer res1.Body.Close()
	defer res2.Body.Close()

	body1, _ := ioutil.ReadAll(res1.Body)
	body2, _ := ioutil.ReadAll(res2.Body)

	fmt.Printf("%.8v\t", string(body1))
	fmt.Printf("%.8v", string(body2))

	//    resp, err := http.Get("https://euro.rate.sx/1xmr")
	// if err != nil {
	//        panic(err)
	// }
	// defer resp.Body.Close()
	//    fmt.Println(resp)
}

func isError(err error) {
    if err != nil{
        panic(err)
    }
}
