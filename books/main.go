package main

import (
	"encoding/json"
	"fmt"
	// "io/ioutil"
	"log"
)

type Book struct {
	Title  string `json:"title"`
	Author string `json:"author"`
}

func main() {
	// file := "test.json"
	// data, err := ioutil.ReadFile(file)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	book := Book{Title: "Hurry!", Author: "Neil DeGrass"}

    byteArray, err := json.Marshal(book)
	if err != nil {
		log.Fatal(err)
	}


	fmt.Println(byteArray, book.Title)

}
