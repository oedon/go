package main

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {

	mkOddDir()
	mkEvenDir()

	var old_loc string = "./jpg/"
	var new_odd string = "./odd/"
	var new_even string = "./even/"

	// var sel string

	f, err := os.Open("./jpg")
	isError(err)
	files, err := f.Readdir(0)
	isError(err)

	list := listFiles(files)                                // make file list
	list_strip := listStrip(list, files)                    // strip .file extension
	moveFiles(list_strip, old_loc, new_odd, new_even, list) // move files

	// Make Menu witch switch
	// for {
	// 	fmt.Printf("CHOOSE OPTION: ")
	// 	fmt.Scan(&sel)
	//
	// 	if sel == "1" {
	// 		list := listFiles(files)                                // make file list
	// 		list_strip := listStrip(list, files)                    // strip .file extension
	// 		moveFiles(list_strip, old_loc, new_odd, new_even, list) // move files
	// 	}
	// 	if sel == "q" {
	// 		break
	// 	}
	// }

}

func moveFiles(list_strip []int, old_loc string, new_odd string, new_even string, list []string) {
	for idx, value := range list_strip {
		if value == 0 {
			fmt.Printf("ERROR: FILE %v of type String not Int\n", list[idx])
			continue
		}
		if value%2 == 0 {
			fmt.Printf("%v \t\t=> %v\n", old_loc+list[idx], new_even+list[idx])
			// fmt.Println(idx, "=>", value, "to even")
			// fmt.Println(old_loc+list[idx], new_even+list[idx])
			err := os.Rename(old_loc+list[idx], new_even+list[idx])
			isError(err)
		}
		if value%2 != 0 {
			fmt.Printf("%v \t\t=> %v\n", old_loc+list[idx], new_odd+list[idx])
			// fmt.Println(idx, "=>", value, "to odd")
			// fmt.Println(old_loc+list[idx], new_odd+list[idx])
			err := os.Rename(old_loc+list[idx], new_odd+list[idx])
			isError(err)
		}
	}
}

func listStrip(list []string, files []fs.FileInfo) []int {
	var list_strip []int
	for idx := range files {
		fileName := fileNameWithoutExtension(list[idx])

		fileconv, err := strconv.Atoi(fileName)
		isError(err)

		list_strip = append(list_strip, int(fileconv))
	}
	return list_strip
}

func listFiles(files []fs.FileInfo) []string {
	var list []string
	for _, v := range files {
		list = append(list, v.Name())
	}
	return list
}

func mkOddDir() {
	_, err := os.Stat("odd")

	oddDir := os.MkdirAll("odd", 0755)
	if oddDir != nil {
		log.Fatal(err)
	}
}

func mkEvenDir() {
	_, err := os.Stat("even")

	evenDir := os.MkdirAll("even", 0755)
	if evenDir != nil {
		log.Fatal(err)
	}
}

func isError(err error) {
	if err != nil {
        log.Println(err)
		// fmt.Println(err)
		// panic(err)
	}
}

func fileNameWithoutExtension(fileName string) string {
	if pos := strings.LastIndexByte(fileName, '.'); pos != -1 {
		return fileName[:pos]
	}
	return fileName
}
