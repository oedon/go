package main

import (
	"fmt"
	// "io/ioutil"
	"log"
	"net/http"
)

func main() {
	data, err := http.Get("https://www.steinacker.com/")
	if err != nil {
		log.Fatal(err)
	}

	// data1, err := ioutil.ReadAll(data.Body)

	defer data.Body.Close() // close the connection

    // fmt.Println(string(data1))
    fmt.Println(data.Status)
    fmt.Println(data.Header)

}
