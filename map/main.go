package main

import (
	"fmt"
)

type store struct {
	name string
	cat  string
}

type account struct {
	equity string
	cur    string
}

type expense struct {
	amount float64
}

func main() {
	aldi := store{
		name: "Aldi",
		cat:  "Groceries",
	}
	lidl := store{
		name: "LIDL",
		cat:  "Groceries",
	}

	stores := []store{aldi, lidl}

	for i, v := range stores {
		fmt.Println(i, v.name)
	}
	fmt.Printf("\"%s\" \"%s\"\n",stores[0].name, stores[0].cat)

}
