package main

import (
	"fmt"
	"math/big"
	// "time"
	"crypto/rand"
)

func main() {
    // GO needs a time seed to change the rand.int. If used with crypto/rand it uses urandom iterface from unix.
    // rand.Seed(time.Now().UnixNano())
    
    a, err := rand.Int(rand.Reader, big.NewInt(100))
    if err != nil {
        fmt.Println(err)
    }
    fmt.Println(a)

}
