package main

import(
    "fmt"
    "math"
    )

func main()  {
    var pi float64 = math.Pi
    var e float64 = math.E
    var phi float64 = math.Phi
    fmt.Printf("%.25v\n", pi)
    fmt.Printf("%g",pi)
    fmt.Printf("%v\n", e)
    fmt.Printf("%v\n", phi)
    fmt.Printf("%v\n", math.Pow(2,16))
    fmt.Printf("%v\n", math.Pow(2,32))
    fmt.Printf("%v\n", math.Pow(2,64))
}
