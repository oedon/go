package main

import (
	"fmt"
	"log"
)

type CalcInterface interface {
	Addition(a int64, b int64) (int64, error)
	Subtraction(a int64, b int64) (int64, error)
	Multiplication(a int64, b int64) (int64, error)
	Division(a float64, b float64) (float64, error)
}

type Adapter struct {
}

func NewAdapter() *Adapter {
	return &Adapter{}
}

func (arith Adapter) Addition(a int32, b int32) (int32, error) {
	return a + b, nil
}
func (arith Adapter) Subtraction(a int32, b int32) (int32, error) {
	return a - b, nil
}
func (arith Adapter) Multiplication(a int32, b int32) (int32, error) {
	return a * b, nil
}
func (arith Adapter) Division(a int32, b int32) (int32, error) {
	return a / b, nil
}

func main() {
	a, err := NewAdapter().Division(25, 11)
    if err != nil {
        log.Fatal(err)
    }

	fmt.Println(a)

}
