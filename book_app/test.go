package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

// MyStruct is an example structure for this program.
type MyStruct struct {
	StructData string `json:"StructData"`
	ISBN       int    `json:"ISBN"`
}

// https://www.sohamkamani.com/golang/json/
func main() {
	filename := "myFile.json"
	err := checkFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	data := []MyStruct{}

	// Here the magic happens!
	json.Unmarshal(file, &data)

	newStruct := &MyStruct{
		StructData: "peanut",
		ISBN:       987627273,
	}

	data = append(data, *newStruct)

	// Preparing the data to be marshalled and written.
	dataBytes, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(filename, dataBytes, 0644)
	if err != nil {
		log.Fatal(err)
	}

	file2 := "myFile.json"
	data2, err := ioutil.ReadFile(file2)
	if err != nil {
		log.Fatal(err)
	}
	// fmt.Println(string(data2))

	var jsonTest []MyStruct
	json.Unmarshal([]byte(data2), &jsonTest)

	// for key, value := range jsonTest {
	//     fmt.Printf("%v %+v\n", key, value)
	//     // for _, v := range value.StructData {
	//     //     fmt.Printf("%v", string(v))
	//     // }
	//
	// }

	for i := 0; i <= len(jsonTest)-1; i++ {
		fmt.Printf("%-10d %s %d\n", i, jsonTest[i].StructData, jsonTest[i].ISBN)
	}

}

func checkFile(filename string) error {
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		_, err := os.Create(filename)
		if err != nil {
			return err
		}
	}
	return nil
}
