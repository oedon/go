package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	// "os"
)

type book struct {
	Title  string
	ISBN   int64
	Author string
}

func main() {

	file := "test.json"

	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	checkValid := json.Valid(data)

	fmt.Println(checkValid)
	fmt.Println(string(data))

// 	f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
//
// 	wString := []byte(`{
// 	"title": "Write TEST",
// 	"ISBN": 2413131341,
// 	"Author": "NOPE"
// }`)
//
// 	f.WriteString(string(wString))
// 	defer f.Close()

    jsonTest := []book{}

	json.Unmarshal(data, &jsonTest)


	fmt.Println(jsonTest.Title)
	fmt.Println(jsonTest.ISBN)
	fmt.Println(jsonTest.Author)

}
