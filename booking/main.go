package main

import (
	"fmt"
	"time"
	// "strconv"
	"strings"
	// "booking/helper"
)

const conferenceTickets uint8 = 50

var conferenceName string = "Go Conference"
var remainingTickets uint8 = 50

// var bookings []string
// var bookings = make([]map[string]string, 0)
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

func main() {
	// var bookings = [50]string{}
	// var bookings [50]string
	// var bookings = []string{}
	// bookings := []string{}

	greetUsers()
	// Capitalise function name from anther package
	// helper.Helptest()

	for {

		firstName, lastName, email, userTickets := getUserInput()

		isValidName, isValidEmail, isValidTicketNumber := validateUserInput(firstName, lastName, email, uint(userTickets), uint(remainingTickets))

		if isValidName && isValidEmail && isValidTicketNumber {

			bookTicket(remainingTickets, userTickets, bookings, firstName, lastName, email)

			// call function first names
			firstNames := getFirstNames(bookings)
			fmt.Printf("These are all our bookings: %v\n", firstNames)

			var noTicketsRemaining bool = remainingTickets == 0

			if noTicketsRemaining == true {
				// end program
				fmt.Println("WE OUT BITCH")
				break
			}
		} else {
			if !isValidName {
				fmt.Println("First name or last name is to short!")
			}
			if !isValidEmail {
				fmt.Println("Wrong Email address!")
			}
			if !isValidTicketNumber {
				fmt.Println("Invaild Ticket Number")
			}
			// Go to the next iteration of the for loop without break
			continue
		}

		// for loop prints index and element of slice
		// for index, element := range bookings{
		//     fmt.Println(index, "=>", element)
		// }

	}

	// city := "London"
	//
	// switch city {
	// case "New York":
	// 	// execute code for new york
	// case "London":
	// 	// execute code for london
	// case "Berlin", "Mexico":
	// 	// execute code for berlin and mexico
	// default:
	// 	// default code block
	// }
}

func greetUsers() {
	fmt.Printf("Welcome to our %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")
}

func getFirstNames(bookings []string) []string {
	firstNames := []string{}
	// _ underscore is a var that you don't want to use
	for _, booking := range bookings {
		// Fields splits string at space
		// var names = strings.Fields(booking)
		// firstNames = append(firstNames, names[0])
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames
}

func validateUserInput(firstName string, lastName string, email string, userTickets uint, remainingTickets uint) (bool, bool, bool) {
	var isValidName bool = len(firstName) >= 2 && len(lastName) >= 2
	var isValidEmail bool = strings.Contains(email, "@")
	var isValidTicketNumber bool = userTickets > 0 && userTickets <= remainingTickets
	return isValidName, isValidEmail, isValidTicketNumber
}

func getUserInput() (string, string, string, uint8) {
	var firstName string
	var lastName string
	var email string
	var userTickets uint8

	// ask user for their name
	fmt.Printf("Enter your first name: ")
	fmt.Scan(&firstName)

	fmt.Printf("Enter your last name: ")
	fmt.Scan(&lastName)

	fmt.Printf("Enter your email: ")
	fmt.Scan(&email)

	fmt.Printf("How many tickets would you like to order?: ")
	fmt.Scan(&userTickets)
	return firstName, lastName, email, userTickets

}

func bookTicket(remainingTickets uint8, userTickets uint8, bookings []string, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	// create a map
	// var userData = make(map[string]string)
    var userData = UserData{
        firstName: firstName,
        lastName: lastName,
        email: email,
        numberOfTickets: uint(userTickets),
    }
	// userData["firstName"] = firstName
	// userData["lastName"] = lastName
	// userData["email"] = email
	// userData["numberOfTickets"] = strconv.FormatUint(uint64(userTickets), 10)

	// bookings[0] = firstName + " " + lastName
	// bookings = append(bookings, firstName+" "+lastName)
	bookings = append(bookings, userData)

	fmt.Printf("User %v %v ordered %v tickets.\n", firstName, lastName, userTickets)
	fmt.Printf("We send a confirmation to %v\n", email)

	fmt.Printf("%v tickets remaining!\n", remainingTickets)
}

func sendTicket(userTickets uint, firstName string, lastName string, email string){
    time.Sleep(10 * time.Second)
    var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)
    fmt.Printf("Sending ticket:\n %v \nto email %v\n", ticket, email)
}
