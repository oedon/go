package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	// HTTP status ranges in a nutshell:
	// 1xx: hold on
	// 2xx: here you go
	// 3xx: go away
	// 4xx: you fucked up
	// 5xx: I fucked up

	url := "https://techcrunch.com"

	response, err := http.Get(url)
	isError(err)
	defer response.Body.Close()

	if response.StatusCode > 400 {
		fmt.Println("Status code:", response.StatusCode)
	}

	doc, err := goquery.NewDocumentFromReader(response.Body)
	isError(err)

	file, err := os.Create("posts.csv")
	isError(err)

	writer := csv.NewWriter(file)

	doc.Find("div.river").Find("div.post-block").Each(func(index int, item *goquery.Selection) {
		h2 := item.Find("h2")
		title := strings.TrimSpace(h2.Text())
		url, _ := h2.Find("a").Attr("href")
		excerpt := strings.TrimSpace(item.Find("div.post-block__content").Text())

		post := []string{title, url, excerpt}

		writer.Write(post)
	})

	writer.Flush()

}

func writeFile(data, filename string) {
	file, err := os.Create(filename)
	isError(err)
	defer file.Close()

	file.WriteString(data)
}

func isError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
