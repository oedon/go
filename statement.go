package main

import (
	"fmt"
)

func main() {
	age := 17

	if age >= 18 {
		fmt.Println("Ride Alone") 
	} else if age >= 14 {
		fmt.Println("Not Alone")
	} else {
		fmt.Println("NOPE")
    }

}
