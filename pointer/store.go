package main

import "fmt"

func main() {

	var store = map[string]string{
		"Aldi": "Groceries",
		"Lidl": "Groceries",
	}

	var storeSel = []string{"Aldi", "Lidl"}

	for i, v := range storeSel {
		fmt.Println(i, v)
	}

	var input int
	fmt.Printf("Select: ")
	fmt.Scan(&input)

	var output string

    pick(input, &output, storeSel, store)

	fmt.Println(output)

}

func pick(input int, output *string, storeSel []string, store map[string]string)  {
	*output = store[storeSel[input]]
}
