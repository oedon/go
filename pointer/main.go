package main

import "fmt"

func main() {
	var point int
	var img = [][]int{{4, 4, 4}, {5, 5, 5}, {6, 6, 6}}

	var img2 = [][]int{{4, 4, 4}, {5, 5, 5}, {6, 6, 6}}

    img = append(img, img2...)

	point = 40
	fmt.Printf("%d %v\n", point, &point)
	pointChange(&point)
	fmt.Printf("%d %v\n", point, &point)

    fmt.Println(img)

	for _, v := range img {
		for j, v := range v {
			fmt.Printf("%v %v \n",j, v)
		}
	}

}

func pointChange(p *int) {
	*p = 42
}
