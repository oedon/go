package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"
)

func main() {
	var currentTime string = time.Now().Format("2006-01-02")

	var loc = []string{"Aldi", "DM", "Fressnapf", "LIDL", "REWE", "Tankstelle Emmelshausen"}
	var dis = []string{"Groceries", "Groceries", "Groceries", "Groceries", "Groceries", "Gas"}
	var exp = []string{"Expenses:Food:Groceries", "Expenses:Car:Gas"}
	var acc = []string{
		"Liabilities:VISA:VW",
		"Liabilities:VISA:SPK",
		"Liabilities:VISA:BCY",
		"Assets:Bank:SPK:Giro",
		"Assets:Bank:VW:Giro"}

	welcome(currentTime)

	st_val := store(loc)
	acc_val := account(acc)
	exp_val := expenses(exp)
	euro := euro()

	data := checkLogic(currentTime, st_val, acc_val, exp_val, loc, dis, acc, exp, euro)

	// checkLogic(currentTime, st_val, acc_val, exp_val, loc, dis, acc, exp)

	fmt.Println(data)

	// readFile("test.beancount")

}

func checkLogic(currentTime string, st_val int, acc_val int, exp_val int, loc []string, dis []string, acc []string, exp []string, euro float32) string {
	var data string
	if st_val <= len(loc)-1 && acc_val <= len(acc)-1 && exp_val <= len(exp)-1 {
		data = fmt.Sprintf("%v * \"%v\" \"%v\"\n  %v\n  %v    %v EUR\n", currentTime, loc[st_val], dis[st_val], acc[acc_val], exp[exp_val], euro)
		fmt.Println("Writing to file...")
		writeFile(data)
	} else {
		fmt.Println("Wrong Store or Account!")
	}
	return data
}

func euro() float32 {
	var euro float32
	fmt.Printf("Enter Amount: ")
	fmt.Scan(&euro)
    // Check logic for . not , in euro amount
	//    isValidAmount := fmt.Sprintf("%f", euro)
	// strings.Contains(isValidAmount, ".")
	return euro
}

func welcome(currentTime string) {
	fmt.Println("Welcome to Beancount")
	fmt.Println("Today is: ", currentTime)
}

func store(loc []string) int {
	var st_val int
	for index, value := range loc {
		fmt.Println(index, "=>", value)
	}
	fmt.Printf("Select Store: ")
	fmt.Scan(&st_val)
	return st_val
}

func account(acc []string) int {
	var acc_val int
	for index, value := range acc {
		fmt.Println(index, "=>", value)
	}
	fmt.Printf("Select Account: ")
	fmt.Scan(&acc_val)
	return acc_val
}

func expenses(exp []string) int {
	var exp_val int
	for index, value := range exp {
		fmt.Println(index, "=>", value)
	}
	fmt.Printf("Select Expenses: ")
	fmt.Scan(&exp_val)
	return exp_val
}

func writeFile(content string) int {
	// TODO os/user allows to get the current user, and for any user, their home directory
	// usr, _ := user.Current()
	// dir := usr.HomeDir
	file, err := os.OpenFile("GO.beancount", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	checkNilErr(err)

	length, err := io.WriteString(file, content+"\n")
	checkNilErr(err)
	defer file.Close()

	return length
}

func readFile(filename string) {
	databyte, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	fmt.Println("Text Data\n", string(databyte))
}

func checkNilErr(err error) {
	if err != nil {
		panic(err)
	}
}
