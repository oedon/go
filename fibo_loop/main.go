package main

import "fmt"

func main() {
    var input int
    fmt.Println("Input max Fibo: ")
    fmt.Scan(&input)

    fiboList := fibo(input)
    fmt.Println(fiboList)

}

func fibo(input int) []int {
	var fiboList []int
	for i := 0; i <= input; i++ {
		if i == 0 {
			fiboList = append(fiboList, i)
		}
		if i == 1 {
			fiboList = append(fiboList, i)
		}
		if i > 1 {
			fiboList = append(fiboList, fiboList[len(fiboList)-2]+fiboList[len(fiboList)-1])
		}
	}
	return fiboList
}
