package main

import (
	"fmt"
)

func main() {
	dataChan := make(chan string)

	var x string
	fmt.Scan(&x)

	go func() {
		dataChan <- x
	}()
    y := <- dataChan

	fmt.Println(y)
}
