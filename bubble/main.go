package main

import (
	"fmt"
)

func main() {

	var list = []int{1, 4, 5, 2, 10, 3, 8, 7}

	for i := 0; i < len(list); i++ {
		for j := 0; j < (len(list) - 1); j++ {
            if list[j] > list[j + 1]{
			temp := list[j]
			list[j] = list[j+1]
			list[j+1] = temp
            }
		}
	}
	fmt.Printf("%v\n", list)

}
