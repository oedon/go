package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

func main() {
	var nameInt int
	name := "5323.jpg"

	if pos := strings.LastIndexByte(name, '.'); pos != -1 {
        var err error
        nameInt, err = strconv.Atoi(name[:pos])
        if err != nil {
            log.Fatal(err)
        }
	}

	fmt.Println(name)
	fmt.Println(nameInt)
}
