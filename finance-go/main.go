package main

import (
	"flag"
	"fmt"
	"os"

	// "github.com/piquette/finance-go"
	"github.com/piquette/finance-go/quote"
	"github.com/sirupsen/logrus"
)

func main() {
    flag.Parse()

    if len(flag.Args()) == 0 {
        logrus.Fatalf("Input one stock symbol", os.Args[0])
    }
    symbol := flag.Args()[0]
    a, _ := quote.Get(symbol)
    fmt.Printf(" --%v--\n", a.ShortName)
    fmt.Printf("Current Price: %v\n", a.Ask)
    fmt.Printf("52WK HIGH: %v\n", a.FiftyTwoWeekHigh)
    fmt.Printf("52WK LOW: %v\n", a.FiftyTwoWeekLow)
    fmt.Printf("200AVG: %v\n", a.TwoHundredDayAverage)
    
}
