package main

import (
	"fmt"
	"reflect"
)

func main() {
	var check float64
	x := true
	y := "string"
	fmt.Println(reflect.TypeOf(x))
	fmt.Println(reflect.TypeOf(y))

	if reflect.TypeOf(y) == reflect.TypeOf(check) {
		fmt.Println("TRUE")

	}

}
func isType(a, b interface{}) bool {
	return fmt.Sprintf("%T", a) == fmt.Sprintf("%T", b)
}
func isType2(a, b interface{}) bool {
	return reflect.TypeOf(a) == reflect.TypeOf(b)
}
